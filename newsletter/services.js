const {
    sendNewsletterMail
} = require('../mailing');

const {
    NewsletterPayload
} = require('./models.js');

const notify = async (mailingList, book) => {
    console.info(`Sending newsletter emails...`);

    const payload = mailingList.map(e => new NewsletterPayload(e, book));

    const promises = payload.map(e => {
        return sendNewsletterMail(e)
    });

    try {
        await Promise.all(promises); //astept sa se trimita toate mail-urile, "in paralel"
    } catch(e) {
        console.info(`There was an error when sending the emails notifications. Please try again later!`);
    }
};

module.exports = {
    notify
}